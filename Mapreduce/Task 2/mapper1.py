#!/usr/bin/env python

import sys
import re

reload(sys)
sys.setdefaultencoding('utf-8')

def proper(word): # проверка принадлежности к имени собственному
    if len(word) >= 6: # длина от 6 до 9 символов
        return True
    if len(word) <= 9:
        return True
    if word.isalpha(): # состоит из букв, причем первая - заглавная, остальные строчные
        return True
    if word[0].isupper():
        return True
    if word[1:].islower():
        return True
    else:
        return False

for line in sys.stdin:
    try:
        article_id, text = unicode(line.strip()).split('\t', 1)
    except ValueError as e:
        continue
    words = re.split('\W*\s+\W*', text, flags = re.UNICODE)
    for word in words:
        name_flag = int(proper(word)) # применяем функцию, проверяем слово
        print ("%d\t%s" % (count, word))
