#!/usr/bin/env bash

OUT_DIR="111_res"
NUM_REDUCERS=8

hadoop fs -rm -r -skipTrash ${OUT_DIR}.tmp > /dev/null

hadoop jar /opt/cloudera/parcels/CDH/lib/hadoop-mapreduce/hadoop-streaming.jar \
    -D mapreduce.job.name="NameCount" \
    -D mapreduce.job.reduces=${NUM_REDUCERS} \
    -files mapper1.py,reducer1.py \
    -mapper "python mapper1.py" \
    -reducer "python reducer1.py" \
    -input /data/wiki/en_articles \
    -output ${OUT_DIR}.tmp > /dev/null

SORT_OUT_DIR="111_sort_res"
NUM_REDUCERS=1

hadoop fs -rm -r -skipTrash ${SORT_OUT_DIR} > /dev/null

hadoop jar /opt/cloudera/parcels/CDH/lib/hadoop-mapreduce/hadoop-streaming.jar \
    -D mapreduce.job.name="CountSorting" \
    -D mapreduce.job.reduces=${NUM_REDUCERS} \
    -D map.output.key.field.separator=\t \
    -D mapreduce.partition.keycomparator.options=-k1,1nr \
    -D mapreduce.job.output.key.comparator.class=org.apache.hadoop.mapreduce.lib.partition.KeyFieldBasedComparator \
    -files mapper2.py,reducer2.py \
    -mapper "python mapper2.py" \
    -reducer "python reducer2.py" \
    -input ${OUT_DIR}.tmp \
    -output ${SORT_OUT_DIR} > /dev/null    

hadoop fs -cat ${SORT_OUT_DIR}/part-00000 | head -n 10
