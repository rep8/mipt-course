#!/usr/bin/env python

import sys
import re

reload(sys)
sys.setdefaultencoding('utf-8')

current_key = None
current_cnt = 0
current_cnt_name = 0

def condition_name(cnt, cnt_name): # если собственные имена повторяются в тексте с маленькой буквы, они нам не подходят
    if (cnt - cnt_name) / cnt * 100 == 0:
        return True
    else:
        return False

for line in sys.stdin:
    try:
        key, cnt, cnt_name = unicode(line.strip()).split('\t')
        cnt = int(cnt)
        cnt_name = int(cnt_name)
    except ValueError as e:
        continue
    
    if current_key != key:
        if current_key and condition_name(current_cnt, current_cnt_name):
            print("%s\t%d" % (current_key, current_cnt))
        current_key = key
        current_cnt = cnt
        current_cnt_name = cnt_name
    else:
        current_cnt += cnt
        current_cnt_name += cnt_name
        
print("%s\t%d" % (current_key, current_cnt))
