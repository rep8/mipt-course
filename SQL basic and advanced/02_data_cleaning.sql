--создадим таблицы для работы, заполним данными--
drop table if exists dataSource;

create table dataSource (
	first_name varchar(255),
	last_name varchar(255),
	email varchar(255),
	gender varchar(255)
);


insert into dataSource (first_name, last_name, email, gender) values 
		(null,                      'Hilda Sodo',           'hsodo1o@surveymonkey.com',                       'F');
insert into dataSource (first_name, last_name, email, gender) values 
		('Torin Cardus',             null,                  'tcardus21@ow.ly',                                'Male');
insert into dataSource (first_name, last_name, email, gender) values 
		(null,                      'Artur MacShane',       'amacshane2d@princeton.edu',                      'M');
insert into dataSource (first_name, last_name, email, gender) values 
		('Freedman Krause',          null,                  'fkrause5t@dagondesign.com',                      'Male');
insert into dataSource (first_name, last_name, email, gender) values 
		(null,                      'Lemmers Remington',    'rlemmers70@tripod.com',                          'Male');
insert into dataSource (first_name, last_name, email, gender) values 
		('Tucker',                  'Crauford',             'tcraufords@chicagotribune.com +7 9346553 221',   'M');
insert into dataSource (first_name, last_name, email, gender) values 
		('Winfield',                'Sharpe',               'wsharpe5k@amazon.co.jp +7-912-321-84-43',        'Male');
insert into dataSource (first_name, last_name, email, gender) values 
		('Caresa',                  'Symmers',              '+79824433556',                                   'F');
insert into dataSource (first_name, last_name, email, gender) values 
		('Rosita',                  'McGing',               'rmcging5@nps.gov 89235428443',                   'Female');
insert into dataSource (first_name, last_name, email, gender) values 
		('Elinor',                  'Barca',                'ebarca54@ning.com 89022338843',                  'Female');
insert into dataSource (first_name, last_name, email, gender) values 
		('Paxon',                   'Rimington',            '89094235643',                   				  'Male');
insert into dataSource (first_name, last_name, email, gender) values 
		('Truda',                   'Biffin',               'tbiffin89@wired.com',                            'F');
insert into dataSource (first_name, last_name, email, gender) values 
		('Noland',                  'Buesden',              '893265432 85',                                   'Male');
insert into dataSource (first_name, last_name, email, gender) values 
		('Brana Champion',           null,                  'bchampiondv@csmonitor.com',                      'Female');

create table dataSource2 (
	first_name varchar(255),
	last_name varchar(255),
	email varchar(255),
	gender varchar(255)
);



insert into dataSource2 (first_name, last_name, email, gender) values ('Rosita',                  'McGing',               'rmcging5@nps.gov 89235428443',                   'Female');                        
insert into dataSource2 (first_name, last_name, email, gender) values ('Elinor',                  'Barca',                'ebarca54@gmail.com 89022338843',                  'Female');                        
insert into dataSource2 (first_name, last_name, email, gender) values ('Paxon',                   'Rimington',            '89094235643',                   				  'Male');                        
insert into dataSource2 (first_name, last_name, email, gender) values ('Truda',                   'Biffin',               'tbiffin81@wired.com 89096450730',                            'F');                 
insert into dataSource2 (first_name, last_name, email, gender) values ('Noland',                  'Buesden',              '893265432 56',                                   'Male');                                    
insert into dataSource2 (first_name, last_name, email, gender) values ('Brana Champion',           null,                  'CBrana@csmonitor.com',                      'Female');


--создадим представление, объединяющее обе таблицы--
CREATE VIEW datasource3 AS
SELECT 
	FIRST_NAME, 
    LAST_NAME, 
    EMAIL, 
    GENDER 
FROM datasource
UNION ALL
SELECT
    FIRST_NAME, 
    LAST_NAME, 
    EMAIL, 
    GENDER 
FROM datasource2; 

SELECT * FROM datasource3;

--Заменим разнообразие значений в поле gender на бинарные--
SELECT
	CASE
		WHEN substr(gender, 1, 1) = 'F' THEN 0
		ELSE 1
	END AS gender,
gender AS column_
FROM datasource3;

/*Заменим значения в атрибутах first_name и last_name на однообразные:
избавимся от сцепленных значений, NULL-значений одиночных значений*/
SELECT 
	CASE
		WHEN strpos(first_name, ' ') = 0 THEN first_name
		WHEN last_name IS NULL THEN substr(first_name, 1, strpos(first_name, ' ')-1)
		WHEN first_name IS NULL THEN substr(last_name, 1, strpos(last_name, ' ')-1)
	END AS new_first_name,
	CASE
		WHEN strpos(last_name, ' ') = 0 THEN last_name
		WHEN first_name IS NULL THEN substr(last_name, strpos(last_name, ' ')+1)
		WHEN last_name IS NULL THEN substr(first_name, strpos(first_name, ' ')+1)
	END AS new_last_name,
	first_name,
	last_name
FROM datasource3;

/* в поле email имеется другая проблема: в части строк указан e-mail, 
в других - телефонный номер, поля для которого вообще нет, причем иногда
телефонный номер дан с пробелами, иногда - без, а в некоторых услаях указан
и email, и телефонный номер */
SELECT
	CASE
		WHEN strpos(email, '@') <> 0 
		THEN CASE
			WHEN strpos(email, ' ') = 0 THEN email
			ELSE substr(email, 0, strpos(email, ' '))
		END
	END AS new_email,
email
FROM datasource3;

--работаем с номерами телефона
SELECT
	CASE 
		WHEN strpos(email, '@') <> 0
		THEN CASE
			WHEN strpos(email, ' ') = 0 THEN NULL
			ELSE substr(email, strpos(email, ' ') + 1)
		END
		else email
	END AS phone,
email
FROM datasource3;

DROP TABLE IF EXISTS datasource_res;
-- собираем запрос в единый, заметив также, что телефоны начинаются с разных кодов --
CREATE TABLE datasource_res AS
SELECT 
first_name,
last_name, 
email,
CASE
	WHEN substr(phone, 1, 1) = '8' THEN '+7' || substr(PHONE, 2)
	ELSE phone
END AS phone,
gender 
FROM(
SELECT 
	CASE
		WHEN strpos(first_name, ' ') = 0 THEN first_name
		WHEN last_name IS NULL THEN substr(first_name, 1, strpos(first_name, ' ')-1)
		WHEN first_name IS NULL THEN substr(last_name, 1, strpos(last_name, ' ')-1)
	END AS first_name,
	CASE
		WHEN strpos(last_name, ' ') = 0 THEN last_name
		WHEN first_name IS NULL THEN substr(last_name, strpos(last_name, ' ')+1)
		WHEN last_name IS NULL THEN substr(first_name, strpos(first_name, ' ')+1)
	END AS last_name,
	CASE
		WHEN strpos(email, '@') <> 0 
		THEN CASE
			WHEN strpos(email, ' ') = 0 THEN email
			ELSE substr(email, 0, strpos(email, ' '))
		END
	END AS email,
	CASE 
		WHEN strpos(email, '@') <> 0
		THEN CASE
			WHEN strpos(email, ' ') = 0 THEN NULL
			ELSE substr(email, strpos(email, ' ') + 1)
		END
		else email
	END AS phone,
	CASE
		WHEN substr(gender, 1, 1) = 'F' THEN 0
		ELSE 1
	END AS gender
FROM datasource3
) t1;
		
--оценим созданную таблицу--
SELECT*
FROM datasource_res;

 /* обратим внимание на недочеты: у поля gender тип int, телефон записан
 через разные знаки. Поправим */
 ALTER TABLE datasource_res ALTER COLUMN gender TYPE CHAR;
 SELECT
 first_name,
 last_name, 
 email,
 replace(replace(phone, '-', ''), ' ', '') as phone,
 gender
 FROM datasource_res;
 
/* Задача подразумевает сдачу в таком виде, но в идеальном состоянии,
но в идеальном виде я бы перевел gender в тип enum */
