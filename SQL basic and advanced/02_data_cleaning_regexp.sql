--создадим таблицы для работы, заполним данными--
drop table if exists dataSource;

create table dataSource (
	first_name varchar(255),
	last_name varchar(255),
	email varchar(255),
	gender varchar(255)
);


insert into dataSource (first_name, last_name, email, gender) values 
		(null,                      'Hilda Sodo',           'hsodo1o@surveymonkey.com',                       'F');
insert into dataSource (first_name, last_name, email, gender) values 
		('Torin Cardus',             null,                  'tcardus21@ow.ly',                                'Male');
insert into dataSource (first_name, last_name, email, gender) values 
		(null,                      'Artur MacShane',       'amacshane2d@princeton.edu',                      'M');
insert into dataSource (first_name, last_name, email, gender) values 
		('Freedman Krause',          null,                  'fkrause5t@dagondesign.com',                      'Male');
insert into dataSource (first_name, last_name, email, gender) values 
		(null,                      'Lemmers Remington',    'rlemmers70@tripod.com',                          'Male');
insert into dataSource (first_name, last_name, email, gender) values 
		('Tucker',                  'Crauford',             'tcraufords@chicagotribune.com +7 9346553 221',   'M');
insert into dataSource (first_name, last_name, email, gender) values 
		('Winfield',                'Sharpe',               'wsharpe5k@amazon.co.jp +7-912-321-84-43',        'Male');
insert into dataSource (first_name, last_name, email, gender) values 
		('Caresa',                  'Symmers',              '+79824433556',                                   'F');
insert into dataSource (first_name, last_name, email, gender) values 
		('Rosita',                  'McGing',               'rmcging5@nps.gov 89235428443',                   'Female');
insert into dataSource (first_name, last_name, email, gender) values 
		('Elinor',                  'Barca',                'ebarca54@ning.com 89022338843',                  'Female');
insert into dataSource (first_name, last_name, email, gender) values 
		('Paxon',                   'Rimington',            '89094235643',                   				  'Male');
insert into dataSource (first_name, last_name, email, gender) values 
		('Truda',                   'Biffin',               'tbiffin89@wired.com',                            'F');
insert into dataSource (first_name, last_name, email, gender) values 
		('Noland',                  'Buesden',              '893265432 85',                                   'Male');
insert into dataSource (first_name, last_name, email, gender) values 
		('Brana Champion',           null,                  'bchampiondv@csmonitor.com',                      'Female');

create table dataSource2 (
	first_name varchar(255),
	last_name varchar(255),
	email varchar(255),
	gender varchar(255)
);



insert into dataSource2 (first_name, last_name, email, gender) values ('Rosita',                  'McGing',               'rmcging5@nps.gov 89235428443',                   'Female');                        
insert into dataSource2 (first_name, last_name, email, gender) values ('Elinor',                  'Barca',                'ebarca54@gmail.com 89022338843',                  'Female');                        
insert into dataSource2 (first_name, last_name, email, gender) values ('Paxon',                   'Rimington',            '89094235643',                   				  'Male');                        
insert into dataSource2 (first_name, last_name, email, gender) values ('Truda',                   'Biffin',               'tbiffin81@wired.com 89096450730',                            'F');                 
insert into dataSource2 (first_name, last_name, email, gender) values ('Noland',                  'Buesden',              '893265432 56',                                   'Male');                                    
insert into dataSource2 (first_name, last_name, email, gender) values ('Brana Champion',           null,                  'CBrana@csmonitor.com',                      'Female');


--создадим представление, объединяющее обе таблицы--
CREATE VIEW datasource3 AS
SELECT 
	FIRST_NAME, 
    LAST_NAME, 
    EMAIL, 
    GENDER 
FROM datasource
UNION ALL
SELECT
    FIRST_NAME, 
    LAST_NAME, 
    EMAIL, 
    GENDER 
FROM datasource2; 

SELECT * FROM datasource3;

CREATE VIEW tmp AS
SELECT
	instr(gender, 'M') as gender,
	regexp_substr(first_name || ' ' || last_name, '[a-z]+', 1, 1, 'i') as first_name,
	/*конкатенируем(||) fn  и ln, находим множество от a до z, с первой строки, 
	первое вхождение, флаг согласования 'i' */
	regexp_substr(first_name || ' ' || last_name, '[a-z]+', 1, 2, 'i') as last_name,
	regexp_substr(email, '\w+@\w+(\.[a-z]+)+') as email,
	/*регулярка: слова или цифры, затем знак @, затем слова или цифры, зачем точки и буквы
	NB! внутри email могут быть домены второго порядка(.co.jp), значит квантификатор [a-z]
	важно повторить, взяв в скобки нужный фрагмент и добавив + */
	regexp_replace(email, '\w+@\w+(\.[a-z]+)+') as email
FROM datasource3;

SELECT
	first_name,
	second_name,
	gender,
	email
	CASE
	WHEN phone is not NULL THEN '+7' 
	END
	|| substr(regexp_replace(phone, '\D'), 2) as phone
FROM tmp
 
/* Для рещения использована нотация Oracle */
 