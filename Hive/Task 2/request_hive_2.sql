ADD jar /opt/cloudera/parcels/CDH/lib/hive/lib/hive-serde.jar;

USE novikov_a;

SELECT date_time, COUNT(1) as cnt FROM Logs_help
GROUP BY date_time
ORDER BY cnt DESC;
