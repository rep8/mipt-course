ADD jar /opt/cloudera/parcels/CDH/lib/hive/lib/hive-serde.jar;

--DROP DATABASE IF EXISTS novikov_a CASCADE;
USE novikov_a;

DROP TABLE IF EXISTS Users;

CREATE EXTERNAL TABLE Users (
        ip STRING,
        browser STRING,
        sex STRING,
        age INT
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.RegexSerDe'
WITH SERDEPROPERTIES (
        "input.regex" = '^(\\S*)\\t(\\S*)\\t(\\S*)\\t(\\d+).*$',
        "output.format.string" = "%1$s %2$s %3$s %4$s"
)
STORED AS TEXTFILE
LOCATION '/data/user_logs/user_data_M';

SELECT * from Users 
LIMIT 10;
