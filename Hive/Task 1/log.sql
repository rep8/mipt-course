ADD jar /opt/cloudera/parcels/CDH/lib/hive/lib/hive-serde.jar;

--DROP DATABASE IF EXISTS novikov_a CASCADE;
USE novikov_a;

DROP TABLE IF EXISTS Logs_help;

CREATE EXTERNAL TABLE Logs_help (
        ip STRING,
        date_time INT,
        http_request STRING,
        size INT,
        http_status INT,
        browser STRING
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.RegexSerDe'
WITH SERDEPROPERTIES (
        "input.regex" = '^(\\S*)\\t\\t\\t(\\d{8})\\S*\\t(\\S*)\\t(\\d*)\\t(\\d*)\\t(\\S*?(?=\\s)).*$'
)
STORED AS TEXTFILE
LOCATION '/data/user_logs/user_logs_M';

--------------------PARTITIONING--------------------
SET hive.exec.dynamic.partition.mode=nonstrict;

USE novikov_a;
DROP TABLE IF EXISTS Logs;

CREATE EXTERNAL TABLE Logs (
    ip STRING,
    http_request STRING,
    size INT,
    http_status INT,
    browser STRING
)
PARTITIONED BY (date_time INT)
STORED AS TEXTFILE;

INSERT OVERWRITE TABLE Logs PARTITION (date_time)

select * from Logs_help limit 10;
