from pyspark import SparkConf, SparkContext
import re

try:
    sc = SparkContext(conf=SparkConf().setAppName("MyApp").setMaster("yarn")) # имя и yarn для запуска через YARN
except:
    pass

def parse(line):   # парсим статьи
    try:
        article_id, text = line.rstrip().split('\t', 1)
        text = re.sub("^\W+|\W+$", "", text, flags=re.UNICODE)
        words = re.split("\W*\s+\W*", text, flags=re.UNICODE)
        return words
    except ValueError as e:
        return []

def bigrams(words): # определяем условие биграм
    pairs = []
    for i in range(len(words)-1):
        if words[i].lower() == 'narodnaya': # сразу привели в нижний регистр
            pair = 'narodnaya_' + words[i+1].lower() # если соответствует 'narodnaya' + следующее слово
            pairs.append((pair, 1))
    return pairs

article = sc.textFile("/data/wiki/en_articles_part/articles-part", 4).map(parse)  # парсим статьи на вход
pairs = article.flatMap(lambda x: bigrams(x)) # ищем биграмы
count = pairs.reduceByKey(lambda x,y: x+y).sortByKey() # сортируем
res = count.collect() # передаем в вывод

for pairs, count in res:
    print("%s\t%d" % (pairs, count))
